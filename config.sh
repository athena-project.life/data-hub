export TF_VAR_org_id=
export TF_VAR_billing_account=
export TF_VAR_environment=production
# export TF_VAR_environment=staging
export TF_ADMIN=dkisler-athena-${TF_VAR_environment}
export TF_CREDS=/tmp/terraform-credentials.json
export GOOGLE_PROJECT=${TF_ADMIN}
export GOOGLE_APPLICATION_CREDENTIALS=${TF_CREDS}
