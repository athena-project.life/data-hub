import os
import time
from typing import Union, Any
from google.cloud import storage, bigquery
from logger import getLogger


PROJECT_ID = os.getenv("PROJECT_ID", "dkisler-athena-staging")

DATA_BUCKET = {
    "raw": os.getenv('DATA_BUCKET_RAW', "athena-datahub-data-raw-staging"),
    "processed": os.getenv('DATA_BUCKET_PROCESSED', "athena-datahub-data-processed-staging"),
    "error": os.getenv('DATA_BUCKET_ERROR', "athena-datahub-data-error-staging"),
}

DATASET = os.getenv("DATASET", "input")


def gcs_move_obj(client: storage.Client,
                 bucket_source: str,
                 bucket_destination: str,
                 obj_source: str,
                 obj_destination: str) -> Union[None, str]:
    """Function to move object between gcp buckets

    Args:
      client: gcs client
      bucket_source: source bucket name
      bucket_destination: destination bucket name
      obj_source: source object name
      obj_destination: destination object name

    Returns:
      error string in case of any
    """
    try:
        bucket_source = client.bucket(bucket_source)
        bucket_destination = client.bucket(bucket_destination)

        blob_source = bucket_source.blob(obj_source)
        blob_copy = bucket_source.copy_blob(
            blob_source, bucket_destination, obj_destination
        )
        
        blob_source.delete()
        
        return
    except Exception as ex:
        return ex


def main(data: dict, context: Any):
    """Cloud function to copy data from gcs to gbq

    Args:
        data: The Cloud Functions event payload
        context: Metadata of triggering event
    """
    obj = data['name']
    uri = f"gs://{DATA_BUCKET['raw']}/{obj}"
    source_id = obj.split("/")[0]
    
    logs = getLogger(logger=f"data-hub-loader")

    try:
        gbq = bigquery.Client(project=PROJECT_ID)
    except Exception as ex:
        logs.send(f"[source: {source_id}] Cannot establish connection to gbq. Error: {ex}",
                  lineno=logs.get_line(),
                  kill=True)
    try:
        gcs = storage.Client(project=PROJECT_ID)
    except Exception as ex:
        logs.send(f"[source: {source_id}] Cannot establish connection to gcs. Error: {ex}",
                  lineno=logs.get_line(),
                  kill=True)
    try:
        buckets_list = [i.id for i in list(gcs.list_buckets())]
    except Exception as ex:
        logs.send("[source: {source_id}] Cannot list buckets. Error:\n{}".format(ex),
                  lineno=logs.get_line(),
                  kill=True)

    buckets_set_required = set(DATA_BUCKET.values())
    if buckets_set_required.intersection(buckets_list) != buckets_set_required:
        buckets_missing = buckets_set_required.difference(buckets_list)
        logs.send(f"""[source: {source_id}] Bucket(s) {','.join(buckets_missing)} don't exist""",
                  lineno=logs.get_line(),
                  kill=True)

    job_config = bigquery.LoadJobConfig()
    job_config.write_disposition = bigquery.WriteDisposition.WRITE_APPEND
    job_config.source_format = bigquery.SourceFormat.NEWLINE_DELIMITED_JSON

    try:
        table_ref = gbq.get_dataset(DATASET)\
                       .table(source_id)
        table = gbq.get_table(table_ref)
    except Exception as ex:
        err = gcs_move_obj(gcs, DATA_BUCKET['raw'], DATA_BUCKET['error'], obj, obj)
        if err:
            logs.send(f"[source: {source_id}] Cannot move from gs://{DATA_BUCKET['raw']}.{obj} to gs://{DATA_BUCKET['error']}.{obj}. Error: {err}",
                      lineno=logs.get_line(),
                      kill=False)
        logs.send(f"[source: {source_id}] Cannot get the table {DATASET}.{source_id}. Error: {ex}",
                  lineno=logs.get_line(),
                  kill=True)

    t0 = time.time()
    
    try:
        load_job = gbq.load_table_from_uri(
            uri,
            table,
            location="US",
            job_config=job_config,
        )
        logs.send(f"[source: {source_id}] Starting job {load_job.job_id}",
                  is_error=False,
                  kill=False)

        while load_job.running():
            time.sleep(0.1)
        
        err = load_job.error_result
        if err:
            raise Exception(err['message'])
        
    except Exception as ex:
        err = gcs_move_obj(gcs, DATA_BUCKET['raw'], DATA_BUCKET['error'], obj, obj)
        if err:
            logs.send(f"[source: {source_id}] Cannot move from gs://{DATA_BUCKET['raw']}.{obj} to gs://{DATA_BUCKET['error']}.{obj}. Error: {err}",
                      lineno=logs.get_line(),
                      kill=False)
        logs.send(f"[source: {source_id}] Cannot load data from {uri} to {DATASET}.{source_id}. Error: {ex}",
                  lineno=logs.get_line(),
                  kill=True)

    err = gcs_move_obj(gcs, DATA_BUCKET['raw'], DATA_BUCKET['processed'], obj, obj)
    if err:
        logs.send(f"[source: {source_id}] Cannot move from gs://{DATA_BUCKET['raw']}.{obj} to gs://{DATA_BUCKET['processed']}.{obj}. Error: {err}",
                  lineno=logs.get_line(),
                  kill=True)
    
    logs.send(f"[source: {source_id}] Completed successfully. Elapsed time: {round(time.time() - t0, 2)} sec. Rows inserted: {load_job.output_rows}.",
              is_error=False)
