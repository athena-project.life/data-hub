resource "google_bigquery_dataset" "preparation" {
  dataset_id    = "preparation"
  friendly_name = "preparation/aggregation/cleaning data layer"
  location      = "US"
  project       = var.project_id

  labels = {
    env   = var.environment
    layer = "preparation"
  }

  access {
    role          = "READER"
    special_group = "projectReaders"
  }
  access {
    role          = "OWNER"
    user_by_email = "terraform@ngo-athena-project-root.iam.gserviceaccount.com"
  }
  access {
    role          = "OWNER"
    special_group = "projectOwners"
  }
}
