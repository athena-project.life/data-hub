resource "google_bigquery_table" "arcgis_de_state_gender_age" {
  project    = var.project_id
  dataset_id = google_bigquery_dataset.input.dataset_id
  table_id   = "arcgis_de_state_gender_age"

  labels = {
    env   = var.environment
    layer = "input"
    test  = true
  }

  time_partitioning {
    type = "DAY"
  }

  schema = <<EOF
[
    {
    "name": "IdBundesland",
    "type": "INT64",
    "mode": "NULLABLE"
  },
  {
    "name": "Bundesland",
    "type": "STRING",
    "mode": "NULLABLE"
  },
  {
    "name": "Landkreis",
    "type": "STRING",
    "mode": "NULLABLE"
  },
  {
    "name": "Altersgruppe",
    "type": "STRING",
    "mode": "NULLABLE"
  },
  {
    "name": "Geschlecht",
    "type": "STRING",
    "mode": "NULLABLE"
  },
  {
    "name": "AnzahlFall",
    "type": "INT64",
    "mode": "NULLABLE"
  },
  {
    "name": "AnzahlTodesfall",
    "type": "INT64",
    "mode": "NULLABLE"
  },
  {
    "name": "ObjectId",
    "type": "INT64",
    "mode": "NULLABLE"
  },
  {
    "name": "Meldedatum",
    "type": "STRING",
    "mode": "NULLABLE"
  },
  {
    "name": "IdLandkreis",
    "type": "STRING",
    "mode": "NULLABLE"
  },
  {
    "name": "Datenstand",
    "type": "STRING",
    "mode": "NULLABLE"
  },
  {
    "name": "NeuerFall",
    "type": "INT64",
    "mode": "NULLABLE"
  },
  {
    "name": "NeuerTodesfall",
    "type": "INT64",
    "mode": "NULLABLE"
  },
  {
    "name": "Refdatum",
    "type": "STRING",
    "mode": "NULLABLE"
  },
  {
    "name": "NeuGenesen",
    "type": "INT64",
    "mode": "NULLABLE"
  },
  {
    "name": "AnzahlGenesen",
    "type": "INT64",
    "mode": "NULLABLE"
  }
]
EOF

}
