resource "google_bigquery_table" "out_smoke_test" {
  project    = var.project_id
  dataset_id = google_bigquery_dataset.distribution.dataset_id
  table_id   = "smoke_test"

  labels = {
    env   = var.environment
    layer = "distribution"
  }

  schema = <<EOF
[
    {
        "name": "cnt_rows",
        "type": "INT64",
        "mode": "REQUIRED"
    },
    {
        "name": "cnt_tz",
        "type": "INT64",
        "mode": "REQUIRED"
    },
    {
        "name": "cnt_ip",
        "type": "INT64",
        "mode": "REQUIRED"
    },
    {
        "name": "run_ts",
        "type": "TIMESTAMP",
        "mode": "REQUIRED"
    }
]
EOF

}
