resource "google_bigquery_dataset" "distribution" {
  dataset_id    = "distribution"
  friendly_name = "output data layer"
  location      = "US"
  project       = var.project_id

  labels = {
    env   = var.environment
    layer = "distribution"
  }

  access {
    role          = "READER"
    special_group = "%{if var.environment == "production"}allAuthenticatedUsers%{else}projectReaders%{endif}"
  }
  access {
    role          = "OWNER"
    user_by_email = "terraform@ngo-athena-project-root.iam.gserviceaccount.com"
  }
  access {
    role          = "OWNER"
    special_group = "projectOwners"
  }
}
