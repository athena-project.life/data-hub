resource "google_bigquery_dataset" "dims" {
  project       = var.project_id
  dataset_id    = "dims"
  friendly_name = "dimensions data"
  location      = "US"

  labels = {
    env   = var.environment
    layer = "dimensions"
  }

  access {
    role          = "READER"
    special_group = "projectReaders"
  }
  access {
    role          = "OWNER"
    user_by_email = "terraform@ngo-athena-project-root.iam.gserviceaccount.com"
  }
  access {
    role          = "OWNER"
    special_group = "projectOwners"
  }
}
