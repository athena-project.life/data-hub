resource "google_bigquery_table" "oxford_gov_responses" {
  project    = var.project_id
  dataset_id = google_bigquery_dataset.input.dataset_id
  table_id   = "oxford_gov_responses"

  labels = {
    env   = var.environment
    layer = "input"
  }

  schema = <<EOF
  [{
    "name": "policyActions",
    "type": "RECORD",
    "mode": "REPEATED",
    "fields": [{
        "name": "policy_type_code",
        "type": "STRING",
        "mode": "REQUIRED"
      },
      {
        "name": "policy_type_display",
        "type": "STRING",
        "mode": "REQUIRED"
      },
      {
        "name": "policyvalue",
        "type": "INT64",
        "mode": "REQUIRED"
      },
      {
        "name": "isgeneral",
        "type": "BOOLEAN",
        "mode": "NULLABLE"
      },
      {
        "name": "notes",
        "type": "STRING",
        "mode": "NULLABLE"
      }
    ]
  },
  {
    "name": "stringencyData",
    "type": "RECORD",
    "mode": "REQUIRED",
    "fields": [{
        "name": "date_value",
        "type": "DATE",
        "mode": "REQUIRED"
      },
      {
        "name": "country_code",
        "type": "STRING",
        "mode": "REQUIRED"
      },
      {
        "name": "confirmed",
        "type": "INT64",
        "mode": "NULLABLE"
      },
      {
        "name": "deaths",
        "type": "INT64",
        "mode": "NULLABLE"
      },
      {
        "name": "stringency_actual",
        "type": "FLOAT64",
        "mode": "NULLABLE"
      },
      {
        "name": "stringency",
        "type": "FLOAT64",
        "mode": "NULLABLE"
      }
    ]
  }
]
EOF
}
