resource "google_bigquery_table" "out_oxford_gov_responses" {
  project    = var.project_id
  dataset_id = google_bigquery_dataset.distribution.dataset_id
  table_id   = "oxford_gov_responses"

  labels = {
    env   = var.environment
    layer = "distribution"
  }

  schema = <<EOF
[{
    "name": "date",
    "type": "DATE",
    "mode": "REQUIRED"
  },
  {
    "name": "country_code",
    "type": "STRING",
    "mode": "REQUIRED"
  },
  {
    "name": "count_infected",
    "type": "INT64",
    "mode": "NULLABLE"
  },
  {
    "name": "count_death",
    "type": "INT64",
    "mode": "NULLABLE"
  },
  {
    "name": "stringencyindex",
    "type": "FLOAT64",
    "mode": "NULLABLE"
  },
  {
    "name": "s1_school_closing",
    "type": "INT64",
    "mode": "NULLABLE"
  },
  {
    "name": "s1_isgeneral",
    "type": "INT64",
    "mode": "NULLABLE"
  },
  {
    "name": "s2_workplace_closing",
    "type": "INT64",
    "mode": "NULLABLE"
  },
  {
    "name": "s2_isgeneral",
    "type": "INT64",
    "mode": "NULLABLE"
  },
  {
    "name": "s3_cancel_public_events",
    "type": "INT64",
    "mode": "NULLABLE"
  },
  {
    "name": "s3_isgeneral",
    "type": "INT64",
    "mode": "NULLABLE"
  },
  {
    "name": "s4_close_public_transport",
    "type": "INT64",
    "mode": "NULLABLE"
  },
  {
    "name": "s4_isgeneral",
    "type": "INT64",
    "mode": "NULLABLE"
  },
  {
    "name": "s5_public_information_campaigns",
    "type": "INT64",
    "mode": "NULLABLE"
  },
  {
    "name": "s5_isgeneral",
    "type": "INT64",
    "mode": "NULLABLE"
  },
  {
    "name": "s6_restrictions_on_internal_movement",
    "type": "INT64",
    "mode": "NULLABLE"
  },
  {
    "name": "s6_isgeneral",
    "type": "INT64",
    "mode": "NULLABLE"
  },
  {
    "name": "s7_international_travel_controls",
    "type": "INT64",
    "mode": "NULLABLE"
  },
  {
    "name": "s8_fiscal_measures",
    "type": "FLOAT64",
    "mode": "NULLABLE"
  },
  {
    "name": "s9_monetary_measures",
    "type": "FLOAT64",
    "mode": "NULLABLE"
  },
  {
    "name": "s10_emergency_investment_in_health_care",
    "type": "FLOAT64",
    "mode": "NULLABLE"
  },
  {
    "name": "s11_investment_in_vaccines",
    "type": "FLOAT64",
    "mode": "NULLABLE"
  },
  {
    "name": "s12_testing_framework",
    "type": "INT64",
    "mode": "NULLABLE"
  },
  {
    "name": "s13_contact_tracing",
    "type": "INT64",
    "mode": "NULLABLE"
  }
]
EOF

}
