resource "google_bigquery_table" "smoke_test" {
  project    = var.project_id
  dataset_id = google_bigquery_dataset.input.dataset_id
  table_id   = "smoke_test"

  labels = {
    env   = var.environment
    layer = "input"
    test  = true
  }

  schema = <<EOF
[
    {
        "name": "week_number",
        "type": "INT64",
        "mode": "REQUIRED"
    },
    {
        "name": "utc_offset",
        "type": "STRING",
        "mode": "REQUIRED"
    },
    {
        "name": "utc_datetime",
        "type": "TIMESTAMP",
        "mode": "REQUIRED"
    },
    {
        "name": "unixtime",
        "type": "INT64",
        "mode": "REQUIRED"
    },
    {
        "name": "timezone",
        "type": "STRING",
        "mode": "REQUIRED"
    },
    {
        "name": "raw_offset",
        "type": "INT64",
        "mode": "REQUIRED"
    },
    {
        "name": "dst_until",
        "type": "TIMESTAMP",
        "mode": "REQUIRED"
    },
    {
        "name": "dst_offset",
        "type": "INT64",
        "mode": "REQUIRED"
    },
    {
        "name": "dst_from",
        "type": "TIMESTAMP",
        "mode": "REQUIRED"
    },
    {
        "name": "dst",
        "type": "BOOLEAN",
        "mode": "REQUIRED"
    },
    {
        "name": "day_of_year",
        "type": "INT64",
        "mode": "REQUIRED"
    },
    {
        "name": "day_of_week",
        "type": "INT64",
        "mode": "REQUIRED"
    },
    {
        "name": "datetime",
        "type": "TIMESTAMP",
        "mode": "REQUIRED"
    },
    {
        "name": "client_ip",
        "type": "STRING",
        "mode": "REQUIRED"
    },
    {
        "name": "abbreviation",
        "type": "STRING",
        "mode": "REQUIRED"
    }
]
EOF

}
