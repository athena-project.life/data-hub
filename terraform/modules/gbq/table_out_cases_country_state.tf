resource "google_bigquery_table" "out_cases_country_state" {
  project    = var.project_id
  dataset_id = google_bigquery_dataset.distribution.dataset_id
  table_id   = "cases_country_state"

  labels = {
    env   = var.environment
    layer = "distribution"
  }

  schema = <<EOF
[
    {
        "name": "date",
        "type": "DATE",
        "mode": "REQUIRED"
    },
    {
        "name": "country_code",
        "type": "STRING",
        "mode": "REQUIRED"
    },
    {
        "name": "country_region",
        "type": "STRING",
        "mode": "NULLABLE"
    },
    {
        "name": "population_region",
        "type": "STRING",
        "mode": "NULLABLE"
    },
    {
        "name": "count_infected",
        "type": "INT64",
        "mode": "REQUIRED"
    },
    {
        "name": "count_recovered",
        "type": "INT64",
        "mode": "NULLABLE"
    },
    {
        "name": "count_dead",
        "type": "INT64",
        "mode": "NULLABLE"
    },
    {
        "name": "count_infected_cumulative",
        "type": "INT64",
        "mode": "REQUIRED"
    },
    {
        "name": "count_recovered_cumulative",
        "type": "INT64",
        "mode": "NULLABLE"
    },
    {
        "name": "count_dead_cumulative",
        "type": "INT64",
        "mode": "NULLABLE"
    }
]
EOF

}
