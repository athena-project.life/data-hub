resource "google_bigquery_table" "out_worldometer_data" {
  project    = var.project_id
  dataset_id = google_bigquery_dataset.distribution.dataset_id
  table_id   = "worldometer_data"

  labels = {
    env   = var.environment
    layer = "distribution"
  }

  schema = <<EOF
[
    {
        "name": "date",
        "type": "DATE",
        "mode": "REQUIRED"
    },
    {
        "name": "country_code",
        "type": "STRING",
        "mode": "REQUIRED"
    },
    {
        "name": "count_total",
        "type": "INT64",
        "mode": "REQUIRED"
    },
    {
        "name": "count_dead_cumulative",
        "type": "INT64",
        "mode": "NULLABLE"
    },
    {
        "name": "count_recovered_cumulative",
        "type": "INT64",
        "mode": "NULLABLE"
    },
    {
        "name": "count_infected_cumulative",
        "type": "INT64",
        "mode": "NULLABLE"
    },
    {
        "name": "count_critial_cumulative",
        "type": "INT64",
        "mode": "NULLABLE"
    },
    {
        "name": "count_tested_cumulative",
        "type": "INT64",
        "mode": "NULLABLE"
    },
    {
        "name": "cases_per_1M_population",
        "type": "INT64",
        "mode": "NULLABLE"
    },
    {
        "name": "deaths_per_1M_population",
        "type": "INT64",
        "mode": "NULLABLE"
    },
    {
        "name": "tests_per_1M_population",
        "type": "INT64",
        "mode": "NULLABLE"
    }
]
EOF

}
