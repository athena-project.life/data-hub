resource "google_bigquery_table" "dim_country" {
  project    = var.project_id
  dataset_id = google_bigquery_dataset.dims.dataset_id
  table_id   = "dim_country"

  labels = {
    env   = var.environment
    layer = "dimensions"
  }

  schema = <<EOF
[   
    {
        "name": "code_iso2",
        "type": "STRING",
        "mode": "REQUIRED"
    },
    {
        "name": "code_iso3",
        "type": "STRING",
        "mode": "REQUIRED"
    },
    {
        "name": "name",
        "type": "STRING",
        "mode": "REQUIRED"
    }
]
EOF

}
