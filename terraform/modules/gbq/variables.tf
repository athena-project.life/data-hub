variable "environment" {
  type    = string
  default = "staging"
}

variable "project_id" {
  type    = string
  default = ""
}
