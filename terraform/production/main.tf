provider "google" {
  credentials = file("/tmp/terraform-credentials.json")
  project     = "dkisler-athena-production"
}

module "gbq" {
  source      = "../modules/gbq"
  environment = var.environment
  project_id  = "${var.project_prefix_data_hub}-${var.environment}"
}
