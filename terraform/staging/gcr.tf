resource "google_cloud_run_service" "service-input-us-1" {
  name     = "data-hub-api-input"
  location = "us-central1"
  project  = "${var.project_prefix_data_hub}-${var.environment}"

  template {
    spec {
      containers {
        image = "gcr.io/${var.project_prefix_data_hub}-${var.environment}/service-interface:${var.docker_tag}"

        env {
          name  = "PROJECT_ID"
          value = "${var.project_prefix_data_hub}-${var.environment}"
        }
        env {
          name  = "DATASET"
          value = "input"
        }
        env {
          name  = "DATA_BUCKET_RAW"
          value = google_storage_bucket.gs-data-raw.name
        }
        env {
          name  = "DATA_BUCKET_ERROR"
          value = google_storage_bucket.gs-data-error.name
        }
        env {
          name  = "SCHEMA_BUCKET"
          value = google_storage_bucket.gs-schema.name
        }
        env {
          name  = "APIKEYS"
          value = var.apikeys
        }

        resources {
          limits = {
            "cpu"    = "1000m"
            "memory" = "1Gi"
          }
        }
      }
      container_concurrency = 2
      service_account_name  = google_service_account.service-input-account.email
    }

    metadata {
      annotations = {
        "autoscaling.knative.dev/maxScale" = "20"
      }
    }
  }

  traffic {
    percent         = 100
    latest_revision = true
  }
}

data "google_iam_policy" "noauth" {
  binding {
    role = "roles/run.invoker"
    members = [
      "allUsers",
    ]
  }
}

resource "google_cloud_run_service_iam_policy" "noauth-service-input-us-1" {
  location = google_cloud_run_service.service-input-us-1.location
  project  = google_cloud_run_service.service-input-us-1.project
  service  = google_cloud_run_service.service-input-us-1.name

  policy_data = data.google_iam_policy.noauth.policy_data
}
