variable "billing_account" {}
variable "org_id" {}
variable "apikeys" {}
variable "docker_tag" {}

variable "environment" {
  type    = string
  default = "staging"
}

variable "project_prefix_data_hub" {
  type    = string
  default = "dkisler-athena"
}

variable "gs_prefix" {
  type    = string
  default = "athena-datahub"
}
