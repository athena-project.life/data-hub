resource "google_storage_bucket" "gs-data-raw" {
  name    = "${var.gs_prefix}-data-raw-${var.environment}"
  project = "${var.project_prefix_data_hub}-${var.environment}"
}

resource "google_storage_bucket" "gs-data-error" {
  name    = "${var.gs_prefix}-data-error-${var.environment}"
  project = "${var.project_prefix_data_hub}-${var.environment}"
}

resource "google_storage_bucket" "gs-data-processed" {
  name    = "${var.gs_prefix}-data-processed-${var.environment}"
  project = "${var.project_prefix_data_hub}-${var.environment}"
}

resource "google_storage_bucket" "gs-schema" {
  name    = "${var.gs_prefix}-schema-${var.environment}"
  project = "${var.project_prefix_data_hub}-${var.environment}"
}

resource "google_storage_bucket" "gs-sql" {
  name    = "${var.gs_prefix}-sql-${var.environment}"
  project = "${var.project_prefix_data_hub}-${var.environment}"
}

resource "google_storage_bucket" "gs-cf" {
  name    = "${var.gs_prefix}-cf-${var.environment}"
  project = "${var.project_prefix_data_hub}-${var.environment}"
}
