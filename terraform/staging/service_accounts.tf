resource "google_service_account" "service-input-account" {
  project      = "${var.project_prefix_data_hub}-${var.environment}"
  account_id   = "run-service-input"
  display_name = "Cloud run service-input account"
}

resource "google_project_iam_member" "service-input-iam" {
  project = "${var.project_prefix_data_hub}-${var.environment}"
  role    = "projects/${var.project_prefix_data_hub}-${var.environment}/roles/${google_project_iam_custom_role.role-run-input.role_id}"
  member  = "serviceAccount:${google_service_account.service-input-account.email}"
}
