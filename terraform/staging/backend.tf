terraform {
  required_providers {
    google = {
      version = "~> 3.15"
    }
  }

  backend "gcs" {
    bucket = "athena-datahub-tf-staging"
    prefix = "terraform/state"
  }
}
