locals {
  cf-loader       = "service-loader"
  filename_on_gcs = "${local.cf-loader}-${data.archive_file.zip-cf-loader.output_md5}.zip"
}


data "archive_file" "zip-cf-loader" {
  type        = "zip"
  source_dir  = "${path.root}/../../gcf/${local.cf-loader}"
  output_path = "${path.root}/../../gcf/${local.cf-loader}.zip"
}

resource "google_storage_bucket_object" "zip-cf-loader" {
  name   = local.filename_on_gcs
  bucket = google_storage_bucket.gs-cf.name
  source = "${path.root}/../../gcf/${local.cf-loader}.zip"
}

resource "google_cloudfunctions_function" "cf-loader" {
  project = "${var.project_prefix_data_hub}-${var.environment}"
  region  = "us-central1"

  name    = "loader_gcs2gbq"
  runtime = "python37"

  service_account_email = google_service_account.service-input-account.email

  source_archive_bucket = google_storage_bucket.gs-cf.name
  source_archive_object = google_storage_bucket_object.zip-cf-loader.name
  entry_point           = "main"

  event_trigger {
    event_type = "google.storage.object.finalize"
    resource   = google_storage_bucket.gs-data-raw.name
  }

  environment_variables = {
    PROJECT_ID            = "${var.project_prefix_data_hub}-${var.environment}"
    DATA_BUCKET_RAW       = google_storage_bucket.gs-data-raw.name
    DATA_BUCKET_PROCESSED = google_storage_bucket.gs-data-processed.name
    DATA_BUCKET_ERROR     = google_storage_bucket.gs-data-error.name
    DATASET               = "input"
  }

  available_memory_mb = 128
  timeout             = 540
  max_instances       = 5
}
