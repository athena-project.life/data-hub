# Infrastructure

## Diagram

![ingestion_pipeline](./fig/pipeline_ingestion.svg)

## API docu

swagger API documentation can be found [here](https://input.production.data-hub.athena-project.life/swagger)
