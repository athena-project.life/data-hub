from functools import wraps
from sanic import request, response
from typing import Union, Tuple
import google.cloud.storage as gcs
from io import BytesIO
from gzip import GzipFile
import json


def authoriziation_check(apikeys: list = []) -> Union[None, str]:
    """Function to check if request is authorized
    
    Args:
      apikeys: list of API keys
    
    Retursn:
      error string in case of any
    """
    def check_api_key(headers: dict,
                      apikeys: list) -> Union[Tuple[str, int], None]:
        """API key handshake
        
        Args:
          headers: request headers
          apikeys: list of API keys
        
        Returns:
          The error string in case of any with the http status code
        """
        if "X-API-KEY" not in headers:
            return "Access forbidden. Specify 'X-API-KEY' request header", 403
        elif headers["X-API-KEY"] not in apikeys:
            return "Access forbidden. Verify correctness of the 'X-API-KEY' request header", 401
        return None
    
    def decorator(f):
        @wraps(f)
        async def decorated_function(request: request,
                                     *args,
                                     **kwargs):
            if apikeys:
                err = check_api_key(request.headers, apikeys)
            else:
                err = None
            if not err:
                res = await f(request, *args, **kwargs)
                return res
            else:
                return response.json({'error': err[0]}, err[1])
        return decorated_function
    return decorator


def array_converter(obj: list) -> Tuple[Union[str, None], 
                                        Union[None, str]]:
    """Function to convert array of json to plain text with one json per line
    
    Args:
      obj: object to convert
    
    Returns:
      output object string and error string in case of any
    """
    try:
        return '\n'.join([json.dumps(i) for i in obj]), None
    except Exception as ex:
        return None, ex
    

def gcs_object_write(obj: dict,
                     bucket: gcs.Bucket,
                     path: str) -> Union[str, None]:
    """Function to write to gcs bucket
    
    Args:
      obj: object to write
      bucket: gcs bucket object
      path: gcs object path
      
    Returns:
      error string in case of any
    """
    to_gzip = path.endswith('gz')
    blob = bucket.blob(path)
    if to_gzip:
        blob.content_encoding = 'gzip'
        with BytesIO() as data_gzip:
            with GzipFile(fileobj=data_gzip,
                          mode='wb') as gz:
                gz.write(obj.encode('utf-8'))
            try:
              blob.upload_from_file(data_gzip,
                                    size=data_gzip.tell(),
                                    rewind=True,
                                    content_type='text/plain')
            except Exception as ex:
              return f"Cannot store data to {path}. Error: {ex}"
    else:
        try:
            blob.upload_from_string(data=obj)
        except Exception as ex:
            return f"Cannot store data to {path}. Error: {ex}"
    return None
