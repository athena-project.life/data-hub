import fastjsonschema


schema = {
    "$schema": "http://json-schema.org/draft-07/schema#",
    "definitions": {
        "element": {
            "type": "object",
            "required": [
                "week_number",
                "utc_offset",
                "utc_datetime",
                "unixtime",
                "timezone",
                "raw_offset",
                "dst_until",
                "dst_offset",
                "dst_from",
                "dst",
                "day_of_year",
                "day_of_week",
                "datetime",
                "client_ip",
                "abbreviation"
            ],
            "properties": {
                "week_number": {
                    "type": "integer",
                    "minimum": 1,
                    "maximum": 53
                },
                "utc_offset": {
                    "type": "string",
                    "description": "Timezone offset to UTC",
                    "examples": [
                        "+02:00"
                    ]
                },
                "utc_datetime": {
                    "type": "string",
                    "format": "date-time",
                    "examples": [
                        "2020-04-05T00:01:04.582658+00:00"
                    ]
                },
                "unixtime": {
                    "type": "integer",
                    "examples": [
                        1586044864
                    ]
                },
                "timezone": {
                    "type": "string",
                    "examples": [
                        "Europe/Berlin"
                    ]
                },
                "raw_offset": {
                    "type": "integer",
                    "examples": [
                        3600
                    ]
                },
                "dst_until": {
                    "type": "string",
                    "format": "date-time",
                    "description": "Daylight saving time clock change, winter time",
                    "examples": [
                        "2020-10-25T01:00:00+00:00"
                    ]
                },
                "dst_offset": {
                    "type": "integer",
                    "examples": [
                        3600
                    ]
                },
                "dst_from": {
                    "type": "string",
                    "description": "Daylight saving time clock change, summer time",
                    "examples": [
                        "2020-03-29T01:00:00+00:00"
                    ]
                },
                "dst": {
                    "type": "boolean",
                    "description": "Is summer time?"
                },
                "day_of_year": {
                    "type": "integer",
                    "minimum": 1,
                    "maximum": 366
                },
                "day_of_week": {
                    "type": "integer",
                    "minimum": 0,
                    "maximum": 6
                },
                "datetime": {
                    "type": "string",
                    "format": "date-time",
                    "examples": [
                        "2020-04-05T02:01:04.582658+02:00"
                    ]
                },
                "client_ip": {
                    "type": "string",
                    "examples": [
                        "127.0.0.1"
                    ]
                },
                "abbreviation": {
                    "type": "string",
                    "description": "Timezone abbreviation",
                    "examples": [
                        "CEST"
                    ]
                }
            },
            "additionalProperties": False,
        },
    },
    "type": "array",
    "items": {
        "$ref": "#/definitions/element"
    }
}

try:
    validator = fastjsonschema.compile(schema)
except fastjsonschema.JsonSchemaDefinitionException as ex:
    print(ex)

data = [
    {
        "week_number": 14,
        "utc_offset": "+02:00",
        "utc_datetime": "2020-04-05T00:20:18.903751+00:00",
        "unixtime": 1586046018,
        "timezone": "Europe/Berlin",
        "raw_offset": 3600,
        "dst_until": "2020-10-25T01:00:00+00:00",
        "dst_offset": 3600,
        "dst_from": "2020-03-29T01:00:00+00:00",
        "dst": True,
        "day_of_year": 96,
        "day_of_week": 0,
        "datetime": "2020-04-05T02:20:18.903751+02:00",
        "client_ip": "79.252.113.180",
        "abbreviation": "CEST",
        "test_column": True
    },
    {
        "week_number": 14,
        "utc_offset": "+02:00",
        "utc_datetime": "2020-04-05T00:23:24.836308+00:00",
        "unixtime": 1586046204,
        "timezone": "Europe/Berlin",
        "raw_offset": 3600,
        "dst_until": "2020-10-25T01:00:00+00:00",
        "dst_offset": 3600,
        "dst_from": "2020-03-29T01:00:00+00:00",
        "dst": True,
        "day_of_year": 96,
        "day_of_week": 0,
        "datetime": "2020-04-05T02:23:24.836308+02:00",
        "client_ip": "79.252.113.180",
        "abbreviation": "CEST"
    },
    {
        "week_number": 14,
        "utc_offset": "+02:00",
        "utc_datetime": "2020-04-05T00:24:09.767341+00:00",
        "unixtime": 1586046249,
        "timezone": "Europe/Berlin",
        "raw_offset": 3600,
        "dst_until": "2020-10-25T01:00:00+00:00",
        "dst_offset": 3600,
        "dst_from": "2020-03-29T01:00:00+00:00",
        "dst": True,
        "day_of_year": 96,
        "day_of_week": 0,
        "datetime": "2020-04-05T02:24:09.767341+02:00",
        "client_ip": "79.252.113.180",
        "abbreviation": "CEST"
    }
]

try:
    validator(data)
except fastjsonschema.JsonSchemaException as ex:
    print(ex)
