import os
import gzip
from typing import List, Tuple, Union
from google.cloud import storage, bigquery
from sanic import Sanic, response
from sanic.exceptions import NotFound
from sanic_openapi import swagger_blueprint, doc
from logger import getLogger
import server.methods as server_methods
import json
import fastjsonschema
import time
from gbqschema_converter.gbqschema_to_jsonschema import sdk_representation as schema_converter


GZIP_HINT = "1f8b"

PROJECT_ID = os.getenv("PROJECT_ID", "test")
DATASET = os.getenv("DATASET", "input")

PORT = os.getenv("PORT", 8080)

DATA_BUCKET = {
    "raw": os.getenv('DATA_BUCKET_RAW', f"athena-data-hub-data-raw"),
    "error": os.getenv('DATA_BUCKET_ERROR', f"athena-data-hub-data-error"),
}

SCHEMA_BUCKET = os.getenv('SCHEMA_BUCKET', f"athena-data-hub-schema")


def api_server() -> Sanic:
  """Function to define web-server"""
  app = Sanic("flat-rent-api")
  app.blueprint(swagger_blueprint)
  app.config["API_CONTACT_EMAIL"] = "contact@athena-project.life?subject=data-hub-api"
  app.config["API_TITLE"] = "Data Hub Interface"
  app.config["API_LICENSE_NAME"] = "MIT"
  app.config["API_VERSION"] = "1.0"
  app.config["API_SCHEMES"] = ["https"]
  app.config["API_BASEPATH"] = os.getenv("SWAGGER_API_BASEPATH", None)
  return app


def get_gbq_table_jsonschema(client: bigquery.Client,
                             table: str) -> Tuple[Union[dict, None], 
                                                  Union[None, str]]:
    """Function to extract gbq table schema
    
    Args:
      client: GBQ client.
      table: Table name.
    
    Returns:
      GBQ table schema and err string in case of any.
    """
    table_schema = None
    try:
        table_ref = client.get_dataset(DATASET)\
                          .table(table)
        table = client.get_table(table_ref)
    except Exception as ex:
        return table_schema, f"Cannot get the table {DATASET}.{table}. Error: {ex}"
    try:
        table_schema = schema_converter(table.schema)
    except Exception as ex:
        return table_schema, f"Cannot get the table {DATASET}.{table}. Error: {ex}"
    return table_schema, None


if __name__ == "__main__":
    logs = getLogger(logger="data-hub-interface")
    
    APIKEYS = os.getenv("APIKEYS", "")
    APIKEYS = APIKEYS.split("|")
    
    try:
        gcs_client = storage.Client(project=PROJECT_ID)
    except Exception as ex:
        logs.send(f"Cannot establish connection to gcs: {ex}", 
                  lineno=logs.get_line(),
                  kill=True)
    
    try:
        gbq_client = bigquery.Client(project=PROJECT_ID)
    except Exception as ex:
        logs.send(f"Cannot establish connection to gbq: {ex}",
                  lineno=logs.get_line(),
                  kill=True)
    
    try:
        buckets_list = [i.id for i in list(gcs_client.list_buckets())]
    except Exception as ex:
        logs.send("Cannot list buckets. Error:\n{}".format(ex), 
                  lineno=logs.get_line(),
                  kill=True)
    
    buckets_set_required = set([*DATA_BUCKET.values(), SCHEMA_BUCKET])
    if buckets_set_required.intersection(buckets_list) != buckets_set_required:
        buckets_missing = buckets_set_required.difference(buckets_list)
        logs.send(f"""Bucket(s) {','.join(buckets_missing)} don't exist""", 
                  lineno=logs.get_line(),
                  kill=True)
    
    bucket_schema = gcs_client.get_bucket(SCHEMA_BUCKET)
    bucket_data_raw = gcs_client.get_bucket(DATA_BUCKET['raw'])
    bucket_data_error = gcs_client.get_bucket(DATA_BUCKET['error'])

    app = api_server()

    @app.exception(NotFound)
    async def wrong_path(request, exception):
        logs.send(f"Wrong route: {request.path}.\nError: {exception}", 
                  lineno=logs.get_line())
        return response.json({"error": f"Wrong route: '{request.path}'"}, 
                             status=404)

    @app.get("/health_check")
    @doc.exclude(True)
    async def status(request) -> response.json:
        """API endpoint for status"""
        return response.json(None, status=200)
      
    @app.post("/post_data")
    @doc.summary("Post data sample to data hub")
    @doc.description("Interface to injest data to the data hub")
    @doc.tag("Post data sample")
    @doc.consumes(doc.String(name="X-API-KEY",
                             description="API auth key"),
                  location="header",
                  required=True)
    @doc.consumes(doc.String(name="source_id",
                             description="Data source id (as agreed with the data-hub team)"),
                  location="query",
                  required=True)
    @doc.consumes(doc.JsonBody(),
                  location="body",
                  required=True)
    @doc.response(200, None, description="Success")
    @doc.response(400, {"error": str}, description="Bad Request")
    @doc.response(401, {"error": str}, description="Unauthorized")
    @doc.response(403, {"error": str}, description="Forbidden")
    @doc.response(404, {"error": str}, description="Not Found")
    @doc.response(415, {"error": str}, description="Unsupported Media Type")
    @doc.response(422, {"error": str}, description="Unprocessable Entity")
    @doc.response(500, {"error": str}, description="Internal Error")
    @server_methods.authoriziation_check(APIKEYS)
    async def post_data(request) -> response.json:
        """API endpoint for the data input"""
        if "source_id" not in request.raw_args:
            return response.json({"error": "specify the 'source_id' parameter in the request query"},
                                 status=400)
        data_source = request.raw_args['source_id']
        # read jsonschema
        try:
            blob = bucket_schema.get_blob(f"data-source/{data_source}.json")
            schema = json.loads(blob.download_as_string())
        except Exception as ex:
            schema, err = get_gbq_table_jsonschema(gbq_client, table=data_source)
            if err:
                logs.send(f"[source: {data_source}] No schema for given data source. Error:\n{ex}",
                          lineno=logs.get_line(),
                          kill=False)
                return response.json({"error": "No schema for given data source"}, 
                                     status=500)
        # extract request body
        request_payload = request.body
        try:
            if request_payload[:2].hex() == GZIP_HINT:
                request_payload = gzip.decompress(request_payload)
        except Exception as ex:
            logs.send(f"[source: {data_source}] Cannot unzip payload. Error:\n{ex}", 
                      lineno=logs.get_line())
            return response.json({"error": "cannot unzip payload"},
                                 status=415)
        # unmarshal json
        try:
            data_input = json.loads(request_payload)
            # clean memory
            del request_payload 
        except Exception as ex:
            logs.send(f"[source: {data_source}] Cannot parse payload data. Error:\n{ex}",
                      lineno=logs.get_line())
            return response.json({"error": "cannot parse payload data"}, 
                                 status=415)
        
        gcs_obj_path = f"{data_source}/{time.strftime('%Y/%m/%d')}/{data_source}_{time.strftime('%Y%m%dT%H%M%S')}.json"
        
        # validate data
        try:
            fastjsonschema.validate(schema, data_input)
            # clean memory
            del schema
        except fastjsonschema.exceptions.JsonSchemaException as ex:
            logs.send(f"[source: {data_source}] Input error. Error:\n{ex}", lineno=logs.get_line())
            err = server_methods.gcs_object_write(obj=json.dumps(data_input), 
                                                  bucket=bucket_data_error,
                                                  path=f"{gcs_obj_path}.gz")
            if err:
                logs.send(err, lineno=logs.get_line())
            return response.json({"error": ex.message}, status=422)
        
        # store data to gcs
        data_output, err = server_methods.array_converter(data_input)
        del data_input
        if err:
            logs.send(err, lineno=logs.get_line())
            err1 = server_methods.gcs_object_write(obj=json.dumps(data_input), 
                                                   bucket=bucket_data_error,
                                                   path=f"{gcs_obj_path}.gz")
            if err1:
                logs.send(err1, lineno=logs.get_line())
            return response.json({"error": "Internal error, please get in touch with data-hub team"}, 
                                 status=500)
        err = server_methods.gcs_object_write(obj=data_output,
                                              bucket=bucket_data_raw,
                                              path=gcs_obj_path)        
        if err:
            logs.send(err, lineno=logs.get_line())
            err1 = server_methods.gcs_object_write(obj=json.dumps(data_input),
                                                   bucket=bucket_data_error,
                                                   path=f"{gcs_obj_path}.gz")
            if err1:
                logs.send(err1, lineno=logs.get_line())
            return response.json({"error": "Internal error, please get in touch with data-hub team"},
                                 status=500)
        return response.json(None, status=200)
        
    app.run(host="0.0.0.0", port=PORT, access_log=False)
