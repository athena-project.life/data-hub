#! /bin/bash

gcloud beta run  \
    deploy data-hub-api-input \
    --platform managed \
    --service-account=run-service-input@${GOOGLE_PROJECT}.iam.gserviceaccount.com \
    --no-allow-unauthenticated \
    --region "us-central1" \
    --image gcr.io/${GOOGLE_PROJECT}/service-interface:latest \
    --memory 2G \
    --concurrency=2 \
    --max-instances=10 \
    --timeout="15m"