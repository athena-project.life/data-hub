#!/bin/bash

env=staging
if [[ "$2" == "production" ]]; then
  env=$2
fi

url=input.${env}.data-hub.athena-project.life

curl -i -H "X-API-KEY: ${APIKEY}" \
  -d @$1 \
  -X POST "https://${url}/post_data?source_id=smoke_test"
